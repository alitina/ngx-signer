import {
  AfterViewInit,
  Component, EventEmitter,
  OnInit, Output,
} from '@angular/core';
import {SIGNER_SETTINGS_FORM} from './signer-settings.form';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'universis-signer-settings',
  templateUrl: './signer-settings.component.html'
})
export class SignerSettingsComponent implements OnInit {

  public model = {
    serviceType: 'SignerService'
  };
  @Output() cancel: EventEmitter<any> = new EventEmitter<any>();
  public readonly formConfiguration = SIGNER_SETTINGS_FORM;

  constructor() {
    const str = localStorage.getItem('user.signer');
    if (str != null) {
      this.model = JSON.parse(str);
    }
  }

  ngOnInit(): void {
  }

  apply() {
    // set local storage
    const update = JSON.stringify({
      serviceType: this.model.serviceType
    });
    localStorage.setItem('user.signer', update);
    // clear session storage (signer authorization)
    sessionStorage.removeItem('signer.auth');
    // reload application
    window.location.reload();
  }

  onCustomEvent(event: { type: string }) {
   if (event.type === 'cancel') {
     this.cancel.emit({
       cancel: true
     });
   } else  if (event.type === 'apply') {
      this.apply();
   }
  }

}
