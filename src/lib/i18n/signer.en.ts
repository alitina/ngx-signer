// tslint:disable:quotemark
// tslint:disable:max-line-length
export const en = {
    "Forms": {
        "Enable digital signing": "Enable digital signing",
        "Report category": "Report catecory",
        "Url": "Url",
        "Use document number": "Use document number (document series)",
        "Publish document": "Publish document"
    },
    "Reports": {
        "Sign": "Sign document",
        "VerificationCode": {
          "Title": "Enter the Remote eSignature OTP code",
          "Label": "Please enter the Remote eSignature OTP code and press Verify to continue",
          "Verify": "Verify"
        }
    },
    "Requests": {
        "Edit": {
          "Start": "Start",
          "ActionMessage": {
            "None": "There is no selected item that meets action requirements.",
            "One": "You have selected one item that meets action requirements. Press Start button to proceed.",
            "Many": "You have selected {{length}} items that meet action requirements. Press Start button to proceed."
          }
        }
    },
    "Documents": {
        "SignError": "An error occurred while signing document. Please try again. Ig the problems persist contact to system administration.",
        "SignInitError": "An error occurred while initializing signing process. Please try again. Ig the problems persist contact to system administration.",
        "SignInProgress": "Document digital signing in progress. Please wait...",
        "RequiredSignature": "Required document signature",
        "UploadSignatureGraphic": "Upload an image of your signature",
        "SignatureGraphicHelp": "This image will be placed alongside your digital signature"
    },
    "Cancel": "Cancel",
    "SignatureVerification": "Verify file digital signature",
    "SignerOfflineCannotVerify": {
      "Title": "Cannot verify digital signature.",
      "Info": "The signer service seems to be offline, or its status cannot be determined.",
      "Suggestion": "You should enable the signer service and then proceed to verify the file's digital signature."
    },
    "FileContainsDigitalSignature": "Digitally signed by",
    "ValidSignature": "(valid)",
    "InvalidSignature": "(invalid)",
    "SubjectInfo": "Certificate owner details",
    "IssuerInfo": "Certificate issuer authority details",
    "CertInfo": "General certificate information",
    "DistNameAttributes": {
      "CommonName": "Name (CN)",
      "OrganizationalUnit": "Organizational Unit (OU)",
      "Organization": "Organization (O)",
      "Locality": "Locality (L)",
      "StateOrProvinceName": "State/Province (ST)",
      "CountryName": "Country (C)"
    },
    "CertificateAttributeLocales": {
      "NotAfter": "Issued on",
      "NotBefore": "Expires on",
      "Expired": "Is expired",
      "SerialNumber": "Serial number",
      "SigAlgName": "Digital signature algorithm",
      "SigAlgOID": "Digital signature algorithm OID",
      "Thumbprint": "Thumbprint",
      "Version": "Version",
      "SigningDate": "Signing Date",
      "Reason": "Reason"
    },
    "FileDoesNotContainSignatures": {
      "Title": "The file does not contain any digital signatures",
      "Info": "The signer service performed the check successfully, but did not manage to find any digital signatures.",
      "Suggestion": ""
    },
    "FoundSignatures": {
      "Title": "Digital signatures have been found in the file",
      "Info": "Below you can see the verification results. Details about the signing certificate, its owner and its issuer are presented.",
      "Suggestion": "By pressing the buttons, the cards containing the information expand."
    },
    "Close": "Close",
    "yes": "Yes",
    "no": "no",
    "Signer": {
      "Settings": "Settings",
      "SettingsTitle": "Signer settings",
      "Services": {
        "HaricaSignerService": "HARICA Cloud Signing",
        "SignerService": "Universis Signer Application"
      }
    }
};
