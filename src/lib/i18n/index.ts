import {el} from './signer.el';
import {en} from './signer.en';

export const SIGNER_LOCALES = {
  el,
  en
};
